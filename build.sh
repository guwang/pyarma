#!/bin/bash

# export TWINE_USERNAME=__token__
# export TWINE_PASSWORD=pypi-AgENdGVzdC5weXBpLm9yZwIkNjFlMmExMzYtMTc2YS00ZmEyLTlkZWItNTkxMzI1MWVhNDVlAAIleyJwZXJtaXNzaW9ucyI6ICJ1c2VyIiwgInZlcnNpb24iOiAxfQAABiDPtpljn9xdvGmhDZrlO81xRmmrBt4ayYnZpuau3HmxTQ
export CIBW_BUILD="cp38-*"
export CIBW_SKIP="*-win32 *-manylinux_i686"
export CIBW_MANYLINUX_X86_64_IMAGE=manylinux2014
export CIBW_BUILD_VERBOSITY=1
export CIBW_BEFORE_ALL="yum install -y openssl11-libs && bash lib/lib.sh"
export CIBW_BEFORE_BUILD="rm -rf _skbuild"
export CIBW_BEFORE_TEST="pip install pytest-cov numpy"
export CIBW_TEST_COMMAND="pytest --cov=pyarma {package}/test/"

sudo -E cibuildwheel --platform linux