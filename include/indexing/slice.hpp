// Copyright 2020 Jason Rumengan, Terry Yue Zhuo
// Copyright 2020 Data61/CSIRO
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ------------------------------------------------------------------------

#pragma once
#include "pybind11/pybind11.h"
#include "armadillo"
#include "subcube.hpp"

namespace py = pybind11;

namespace pyarma {
    template<typename T>
    arma::subview_cube<typename T::elem_type> get_slice(const T &cube, const std::tuple<py::slice, py::slice, arma::uword> coord);

    template<typename T, typename U>
    // Same idea with tuple<int, py::slice> that takes one slice or one slice that's column-limited
    void set_slice(T &cube, std::tuple<py::slice, py::slice, arma::uword> coord, U item);
}
