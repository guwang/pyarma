# Based on https://github.com/mlpack/mlpack-wheels

$basedir=$(py lib/openblas_support.py)
cp $basedir lib

$basedir=$(py lib/gfortran_support.py)
cp $basedir/Library/mingw-w64/bin/libgfortran-3.dll lib

# $basedir=<py lib/hdf5_support.py>
# cp -r $basedir/lib/* /usr/local/lib
# cp $basedir/include/* /usr/local/include
# cp $basedir/bin/* /usr/local/bin