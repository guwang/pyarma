REQUIRE\( (.+)\((.+)\) == Approx\((.+)\) \)

assert math.isclose($1[$2], $3) == True

TEST_CASE\("(.+)"\)

def test_$1():

fill::
pa.fill.

mat (.)
$1 = pa.mat

REQUIRE\( (.+) == Approx\(.+\).epsilon\((.+)\) \)

assert math.isclose($1, $2, abs_tol=$3) == True

REQUIRE\( (.+) == Approx\((.+)\)[\s]+\)

assert math.isclose($1, $2) == True


# Spans
span\(([0-9]+),([0-9]+)\)

$1:$2

# Head/tail rows/columns
.(head|tail)_(rows|cols)\(([0-9]+)\)

[pa.$1_$2, $3]

# Columns
.col\(([0-9]+)\)
[:, $1]

# Rows
.row\(([0-9]+)\)
[$1, :]

# Main diagonals
.diag\(\)
[diag]
