from pyarma import *

m = m = cube(2,3,4,fill.ones)
m.print()

n = m = cube(2,3,4,fill.zeros)
n.print()

o = m = cube(2,3,4,fill.randu)
o.print()

q = m = cube(2,3,4,fill.randn)
q.print()

r = m = cube(2,3,4,fill.none)
r.print()