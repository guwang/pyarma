from pyarma import *

m = mat(2,3,fill.ones)
m.print()

n = mat(2,3,fill.zeros)
n.print()

o = mat(2,3,fill.randu)
o.print()

p = mat(3,3,fill.eye)
p.print()

q = mat(2,3,fill.randn)
q.print()

r = mat(2,3,fill.none)
r.print()